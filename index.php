<?php
ini_set('log_errors', 1);
ini_set('error_log', 'tmp/php.log');

require __DIR__ . '/service/titleService.php';

?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Generador de Pointer</title>

</head>

<body>
  <div class="container" style="display:flex; flex-wrap: wrap; justify-content: center; ">
    <div class="fileList" style="margin: 16px">
      <h1>Lista de pointers</h1>
      <div>
        <h3>Como usar?</h3>
        <ul>
          <li>Elige el documento pointer que quieras editar de la siguiente lista</li>
          <li>Editalo y en seguida el mismo archivo se sobre escribirá </li>
        </ul>

        <h2>Lista de archivos</h2>
        <?php
        $path    =  __DIR__ . '/pointers';
        $list = scandir($path);

        if (!empty($list)) {
          echo "<ul>";
          foreach ($list as $f) {
            if (!is_dir($path . DIRECTORY_SEPARATOR . $f) && $f != ".." && $f != ".") {
              echo "<li><a href=\"pointers/$f\">$f</a></li>";
            }
          }
          echo "</ul>";
        }

        ?>
      </div>
    </div>

    <div class="titleList" style="margin: 16px;">
      <h1>Generador de pointers</h1>

      <div class="titleInput">
        <h3>Agrega un Titulo</h3>
        <form action="service/titleService.php" method="post" id="form_title">
          <input type="text" placeholder="Título..." name="title" id="title" required>
          <button type="submit" value="Agregar" form="form_title"> Agregar </button>
        </form>
      </div>

      <div class="listContainer">
        <h3>Lista de Pointers Disponibles</h3>
        <ul id="titleList">
          <?php
          $titles = getTitleArray("storage/titleStorage.json");
          foreach ($titles as $key => $value) {
            echo
            "<li>
              <a href=\"./pointers/pointer_$value.html\">$value</a>
              <form action=\"service/deleteService.php\" method=\"post\" id=\"form_delete_$key\">
              <input type=\"hidden\" value=\"$key\" name=\"id\"/>
              <button type=\"submit\" value=\"Borrar\" form=\"form_delete_$key\"> Borrar </button>
              </form>
            </li>";
          }
          ?>
        </ul>
      </div>
    </div>

  </div>

</body>

</html>