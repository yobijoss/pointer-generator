<?php

function generatePointer(string $title)
{
  $template = file_get_contents('./template.html');
  $template = str_replace("<--POINTER_TITLE-->", $title, $template);


  if (!is_dir('../pointers')) {
    mkdir('../pointers');
  }

  file_put_contents('../pointers/pointer_' . $title . '.html', $template);
}
