<?php

require __DIR__ . '/ponterFileGenerator.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: POST, OPTIONS");
    }

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    }
}


function getTitleArray(string $fileName = '../storage/titleStorage.json'): array
{
    if (!file_exists($fileName)) {
        return [];
    }

    try {
        $jsonArray = file_get_contents($fileName);
        return json_decode($jsonArray, true, 512, JSON_THROW_ON_ERROR);
    } catch (Exception $e) {
        error_log("Hubo un error al parsear el archivo $fileName, reiniciando...", 0);
    }
    return [];
}

function saveArray(array $titles, string $fileName = '../storage/titleStorage.json')
{
    if (!is_dir('../storage')) {
        mkdir('../storage');
    }

    try {
        $jsonString = json_encode($titles);
        file_put_contents($fileName, $jsonString);
    } catch (Exception $e) {
        error_log("Error al salvar el archivo, revisa los permisos", 0);
    }

        
    header("Location: http://localhost:8089",  true,  302);
    exit(0);
}

$array = getTitleArray();

if (isset($_POST['title'])) {
    if (!empty($_POST['title'])) {
        array_push($array, strtoupper($_POST['title']));
        generatePointer($_POST['title']);
        saveArray($array);
    }
}



