<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        
  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
      // may also be using PUT, PATCH, HEAD etc
      header("Access-Control-Allow-Methods: POST, OPTIONS");         
  
  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
      header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

  exit(0);
}


if(!isset($_POST['filePath'])) {
  $output["status"] = 400;
  $output["message"] = "No se envio el contenido :/";
  http_response_code(400);
  echo json_encode($output);
  return;
}
$filePath = $_POST['filePath'];
$fileContent = $_POST['fileContent'];

$indexFile = fopen("..".$filePath, "w") or die("No se puede abrir el archivo :/");

fwrite($indexFile, $fileContent);
fclose($indexFile);


// recibes /pointers/pointer-01.html

$output["status"] = 200;
$output["message"] = "Archivo Salvado generado exitosamente";
http_response_code(200);
echo json_encode($output);