//Cada caracter de .title se convierte a un span
$("#title").lettering();

/* Elementos html */
var title = document.getElementById("title");
var titleInput = document.getElementById("titleInput");
var radiusSlider = document.getElementById("radiusSlider");
var letters = document.querySelectorAll("h1 span");

/* Elementos SVG */
var myCircle = document.getElementById("mainCircle");
var myPath = document.getElementById("mainLine");
var handle1 = document.getElementById("handle1");

/* Variabeles exclusivas del círculo */
var circlePosition = { x: 0, y: 0 };
circlePosition.x = parseFloat(mainCircle.getAttribute("cx"));
circlePosition.y = parseFloat(mainCircle.getAttribute("cy"));

var circleRadius = mainCircle.getAttribute("r");

/* Variabeles exclusivas de la línea */

var pathValue = myPath.getAttribute("d");
console.log("d = " + pathValue);
var pathArray = pathValue.split(" L ");
console.log(pathArray);
var pathL1Array = pathArray[1].split(" ");
console.log(pathL1Array);
console.log(pathL1Array[0]);

var pathL2Array = pathArray[2].split(" ");
console.log(pathL2Array);
console.log(pathL2Array[0]);

var pathM = { x: circlePosition.x, y: circlePosition.y };
var pathL1 = { x: 0, y: 0 };
pathL1.x = parseFloat(pathL1Array[0]);
pathL1.y = parseFloat(pathL1Array[1]);
var pathL2 = { x: 0, y: pathL1.y };
pathL2.x = parseFloat(pathL2Array[0]);

/* Variables comunes para los elementos del SVG */
var strokeWidth = 10;
var strokeColor = "#f5ff00";
var strokeLineJoin = "round";
var strokeLineCap = "round";
var fill = "none";

var tangentPoint = { x: 0, y: 0 };
var tangentPointPosition;
var tangentPointRight = { x: circlePosition.x + circleRadius, y: pathM.y };
var tangentPointLeft = { x: circlePosition.x - circleRadius, y: pathM.y };
var tangentPointTop = { x: pathM.x, y: circlePosition.y + circleRadius };
var tangentPointBottom = { x: pathM.x, y: circlePosition.y - circleRadius };

myCircle.setAttribute("stroke-width", strokeWidth);
myCircle.setAttribute("stroke", strokeColor);
myCircle.setAttribute("stroke-linejoin", strokeLineJoin);
myCircle.setAttribute("stroke-linecap", strokeLineCap);
myCircle.setAttribute("fill", fill);
myCircle.setAttribute("cx", circlePosition.x);
myCircle.setAttribute("cy", circlePosition.y);

myCircle.style.strokeDasharray = Math.floor(circleLenght(circleRadius));

myPath.setAttribute("stroke-width", strokeWidth);
myPath.setAttribute("stroke", strokeColor);
myPath.setAttribute("stroke-linejoin", strokeLineJoin);
myPath.setAttribute("stroke-linecap", strokeLineCap);
myPath.setAttribute("fill", fill);

handle1.setAttribute("cx", pathL1.x);
handle1.setAttribute("cy", pathL1.y);
handle1.setAttribute("r", strokeWidth);
handle1.setAttribute("fill", strokeColor);

radiusSlider.setAttribute("data-value", circleRadius);

findBestTangentPoint();
setCircleRotation();
updateStrokeDasharray();
setPath();
animatePath();

/* INTERACT */

interact("#handle1")
  .draggable({
    listeners: {
      start(event) {
        console.log(event.type, event.target);
      },
      move(event) {
        pathL1.x += event.dx;
        pathL1.y += event.dy;
        pathL2.y += event.dy;
      },
    },
  })
  .on("dragmove", function (event) {
    setPath();
    findBestTextDirection();
    handle1.setAttribute("cx", pathL1.x);
    handle1.setAttribute("cy", pathL1.y);
    findBestTangentPoint();
    setCircleRotation();
    textTopPosition();
    updateStrokeDasharray();
  })
  .on("dragend", function (event) {
    setCircleRotation();
    animatePath();
    documentUpdate(); //Guardar documento
  });

interact("#mainCircle")
  .draggable({
    listeners: {
      start(event) {
        console.log(event.type, event.target);
      },
      move(event) {
        circlePosition.x += event.dx;
        circlePosition.y += event.dy;
      },
    },
  })
  .on("dragmove", function (event) {
    myCircle.setAttribute("cx", circlePosition.x);
    myCircle.setAttribute("cy", circlePosition.y);
    circleRadius = myCircle.getAttribute("r");

    findBestTangentPoint();
    setCircleRotation();
    setPath();
    findBestTextDirection();
    textTopPosition();
    updateStrokeDasharray();
  })
  .on("dragend", function (event) {
    setCircleRotation();
    animatePath();
    documentUpdate(); //Guardar documento
  });

const slider = interact(".slider"); // target elements with the "slider" class

slider
  // Step 2
  .draggable({
    // make the element fire drag events
    origin: "self", // (0, 0) will be the element's top-left
    inertia: true, // start inertial movement if thrown
    modifiers: [
      interact.modifiers.restrict({
        restriction: "self", // keep the drag coords within the element
      }),
    ],
    // Step 3
    listeners: {
      move(event) {
        // call this listener on every dragmove
        const sliderWidth = interact.getElementRect(event.target).width;
        const value = event.pageX / sliderWidth;

        event.target.style.paddingLeft = value * 100 + "%";
        event.target.setAttribute("data-value", value.toFixed(2));
      },
    },
  })
  .on("dragmove", function (event) {
    circleRadius = event.pageX.toFixed(2);
    myCircle.setAttribute("r", circleRadius);
    findBestTangentPoint();
    findBestTextDirection();
    setPath();
    textTopPosition();

    myCircle.style.strokeDasharray = Math.floor(circleLenght(circleRadius));

    updateStrokeDasharray();

    console.log("Circunferencia: " + Math.floor(circleLenght(circleRadius)));
  })
  .on("dragend", function (event) {
    animatePath();
    setCircleRotation();
    documentUpdate(); //Guardar documento
  });

function documentUpdate() {
  /* Función que guarda el archivo HTML */

  console.log("Guardar documento");

  var yourDOCTYPE = "<!DOCTYPE html>"; // your doctype declaration

  var html =
    yourDOCTYPE + "<html>" + document.documentElement.innerHTML + "</html>";

  const data = new FormData();
  data.append("filePath", window.location.pathname);
  data.append("fileContent", html);

  console.log(window.location.pathname);
  fetch("http://localhost:8089/service/autosave.php", {
    method: "POST",
    body: data,
    mode: "cors",
    headers: {
      Origin: "notifest",
    },
  })
    .then(function (response) {
      if (response.ok) {
        console.log("archivo salvado exitosamenete");
      } else {
        console.error(response);
        alert("Algo falló, podrías ver los logs?");
      }
    })
    .catch(function (err) {
      console.error(err);
      alert("Asegurate de iniciar start.sh en la raiz del proyecto");
    });
}

function setPath() {
  myPath.setAttribute(
    "d",
    "M " +
      tangentPoint.x +
      " " +
      tangentPoint.y +
      " L " +
      pathL1.x +
      " " +
      pathL1.y +
      " L " +
      pathL2.x +
      " " +
      pathL2.y
  );
}

function findBestTextDirection() {
  if (pathL1.x >= circlePosition.x) {
    title.style.left = pathL1.x + "px";
    title.style.padding = "0 0 0 14px";
    pathL2.x = pathL1.x + title.offsetWidth;
  } else {
    pathL2.x = pathL1.x - title.offsetWidth;
    title.style.left = pathL2.x + "px";
    title.style.padding = "0 14px 0 0";
  }
}

function textTopPosition() {
  title.style.top = pathL1.y - title.offsetHeight - 3 + "px";
}

function findBestTangentPoint() {
  if (circlePosition.x + circleRadius * 1 > pathL1.x) {
    tangentPointPosition = "top";
    tangentPoint.x = circlePosition.x;
    tangentPoint.y = circlePosition.y - circleRadius * 1;
  }

  if (
    circlePosition.x + circleRadius * 1 > pathL1.x &&
    circlePosition.y + circleRadius * 1 < pathL1.y
  ) {
    tangentPointPosition = "bottom";
    tangentPoint.x = circlePosition.x;
    tangentPoint.y = circlePosition.y + circleRadius * 1;
  }

  if (circlePosition.x - circleRadius * 1 > pathL1.x) {
    tangentPoint.x = circlePosition.x - circleRadius * 1;
    tangentPoint.y = circlePosition.y;
    tangentPointPosition = "left";
  }

  if (circlePosition.x + circleRadius * 1 < pathL1.x) {
    tangentPoint.x = circlePosition.x + circleRadius * 1;
    tangentPoint.y = circlePosition.y;
    tangentPointPosition = "right";
  }

  pathM.x = tangentPoint.x;
  pathM.y = tangentPoint.y;
}

function setCircleRotation() {
  var rotation;

  switch (tangentPointPosition) {
    case "top":
      rotation =
        "rotate(-90 " + circlePosition.x + " " + circlePosition.y + ")";

      break;
    case "bottom":
      rotation = "rotate(90 " + circlePosition.x + " " + circlePosition.y + ")";

      break;
    case "left":
      rotation =
        "rotate(180 " + circlePosition.x + " " + circlePosition.y + ")";

      break;
    case "right":
      rotation = "rotate(0 " + circlePosition.x + " " + circlePosition.y + ")";
      break;
  }

  myCircle.setAttribute("transform", rotation);
  console.log(tangentPointPosition + " " + rotation);
}

titleInput.addEventListener("keyup", function (event) {
  if (event.keyCode === 13) {
    event.preventDefault();
    titleInput.setAttribute("value", title.innerHTML);
    title.innerHTML = titleInput.value;
  }
});

function changeText(element) {
  title.innerHTML = element.value;
  titleInput.setAttribute("value", title.innerHTML);
  setPath();
  findBestTextDirection();

  $("#title").lettering();
  var letters = document.querySelectorAll("h1 span");
  console.log(letters);

  for (var i = 0; i < letters.length; i++) {
    var c = letters[i];
    c.style.opacity = 1;
    c.style.bottom = "0px";
  }

  documentUpdate(); //Guardar documento
}

function pathLenght(start, end) {
  let a = start.x - end.x;
  let b = start.y - end.y;
  return Math.sqrt(a * a + b * b);
}

function circleLenght(radio) {
  return 2 * Math.PI * radio;
}

function updateStrokeDasharray() {
  //console.log('distance: ' + pathLenght(pathM, pathL1) + ', ' + pathLenght(pathL1, pathL2) + ' = ' + (pathLenght(pathM, pathL1)+pathLenght(pathL1, pathL2)) );
  myPath.style.strokeDasharray =
    pathLenght(pathM, pathL1) + pathLenght(pathL1, pathL2);
}

function animatePath() {
  for (var i = 0; i < letters.length; i++) {
    var c = letters[i];
    c.style.opacity = 0;
    c.style.bottom = "-50px";
  }

  var tl = anime.timeline({ easing: "linear", duration: 1000 });

  tl.add({
    targets: "#mainCircle",
    strokeDashoffset: [anime.setDashoffset, 0],
  })
    .add({
      targets: "#mainLine",
      strokeDashoffset: [anime.setDashoffset, 0],
    })
    .add(
      {
        targets: letters,

        bottom: 0,
        opacity: 1,
        delay: anime.stagger(50),
        duration: 800,
        easing: "easeOutBack",
      },
      "-=800"
    );
}
