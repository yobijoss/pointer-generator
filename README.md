# Generador de pointers para Notifest!

## Como usar?

1. COpia este directorio en donde tu quieras
2. asegurate de darle permisos al archivo `start.sh`
  - Ejecuta el siguiente comando: `sudo chmod 777 start.sh`
3. Ejecuta `./start.sh` en la raiz del terminal
4. Abre la siguiente dirección en el navegador: http://localhost:8089
5. Eligue de la lista el pointer que quieras editar, para crear uno nuevo solo agregalo en la carpeta `/pointers`

Listo! Hora de crear algunos pointers